<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\First;
use AppBundle\Entity\Product;
use AppBundle\Entity\Category;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/new", name="new")
     */
    public function createAction()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $about = new First();
        $about->setName('Pawel');
        $about->setAddress('Wroclaw');

        $entityManager->persist($about);
        $entityManager->flush();

        return new Response('Zapisano jako id '.$about->getId());
    }

    /**
     * @Route("/show/{id}", name="show")
     */
    public function showFirstAction($id)
    {
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:First');

        $about = $repository->find($id);
        return new Response('Wyświetlono id '.$about->getId());
    }

    /**
     * @Route("/update/{id}", name="update")
     */
    public function updateFirstAction($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $about = $entityManager->getRepository('AppBundle:First')->find($id);

        if (!$about) {
            throw $this->createNotFoundException(
                'Nie znaleziono wpisu o id '.$id
            );
        }

        $about->setAddress('Katowice');
        $entityManager->flush();

        return new Response('Zaktualizowano id '.$about->getId().
            ' na miasto '. $about->getAddress());

    }

    /**
     * @Route("/show_product/{id}", name="show")
     */
    public function showProductAction($id)
    {
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Product');

        $product = $repository->find($id);
        return new Response('Wyświetlono id '.$product->getId(). ' Produkt: '.$product->getName());
    }

}
